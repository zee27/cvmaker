import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import { Book } from './Book';
import { BookService } from '../book.service'; 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [ './dashboard.component.css' ]
})
export class DashboardComponent implements OnInit {
  heroes: Hero[] = [];
  books: Book[];
  

  constructor(private heroService: HeroService ,  private bookService: BookService) { }
 
 
  ngOnInit() {
    this.getHeroes();
    this
    .bookService
    .getBooks()
    .subscribe((data: Book[]) => {
      this.books = data;
  });

}


  getHeroes(): void {
    this.heroService.getHeroes()
      .subscribe(heroes => this.heroes = heroes.slice(1, 5));
  }
}