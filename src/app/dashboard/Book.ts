﻿export interface Book {
    userId: Number;
    id: Number;
    title: String;
    body: String;
}