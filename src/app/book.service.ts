﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BookService {

  url = 'https://jsonplaceholder.typicode.com';
  url1 = 'http://192.168.0.100:8080';
  constructor(private http: HttpClient) { }

  getBooks() {
    return this
            .http
           // .get(`${this.url}/posts`);
           .get(`${this.url1}/aliens`);
        }
}